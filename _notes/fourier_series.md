---
title: Fourier Series
---

## Definition

Let $$f: \mathbb{R} \rightarrow \mathbb{C}$$ be a reasonably well behaved $$L$$-periodic function
(i.e. $$f(x) = f(x + L)$$ for all $$x$$). Then $$f$$ can be expressed as an infinite series

$$
f(x) = \sum_{n \in \mathbb{Z}} A_n e^{2 \pi i n x / L}
$$

The values of the coefficients $$A_n$$ can be found using the orthogonality of basic exponential
sinusoids. For any $$m \in \mathbb{Z}$$,

$$
\begin{align*}
\int_{-L/2}^{L/2} f(x) e^{-2 \pi i m x / L} dx
&= \int_{-L/2}^{L/2} \sum_{n \in \mathbb{Z}} A_n e^{2 \pi i n x / L} e^{-2 \pi i m x / L} dx \\
&= \sum_{n \in \mathbb{Z}} A_n \int_{-L/2}^{L/2} e^{2 \pi i (n - m) x / L} dx \\
&= \sum_{n \in \mathbb{Z}} A_n L \delta_{nm} \\
&= L A_m
\end{align*}
$$

where the second to last line uses [Kronecker delta](https://en.wikipedia.org/wiki/Kronecker_delta)
notation. Thus by the periodicity of $$f$$ and the exponential function

$$
\begin{align*}
A_n &= \frac{1}{L} \int_{-L/2}^{L/2} f(x) e^{-2 \pi i n x / L} dx \\
&= \frac{1}{L} \int_{0}^{L} f(x) e^{-2 \pi i n x / L} dx
\end{align*}
$$


## Relation to the Fourier Transform

Now suppose we have a function $$f: \mathbb{R} \rightarrow \mathbb{C}$$, and we construct an
$$L$$-periodic version

$$
f_L(x) = \sum_{n \in \mathbb{Z}} f(x + nL)
$$

The coefficients of the Fourier Series of $$f_L$$ are

$$
\begin{align*}
L A_n &= \int_0^L \sum_{m \in \mathbb{Z}} f(x + mL) e^{-2 \pi i n x / L} dx \\
&= \sum_{m \in \mathbb{Z}} \int_0^L f(x + mL) e^{-2 \pi i n x / L} dx \\
&= \sum_{m \in \mathbb{Z}} \int_{mL}^{(m + 1)L} f(x) e^{-2 \pi i n (x - mL) / L} dx \\
&= e^{-2 \pi i n m} \sum_{m \in \mathbb{Z}} \int_{mL}^{(m + 1)L} f(x) e^{-2 \pi i x n / L} dx \\
&= \int_\mathbb{R} f(x) e^{-2 \pi i x n / L} dx \\
&= \hat{f}(n/L)
\end{align*}
$$

so

$$
f_L(x) = \frac{1}{L} \sum_{n \in \mathbb{Z}} \hat{f}(n / L) e^{2 \pi i n x / L}
$$

Thus the periodic summation of $$f$$ is completely determined by discrete samples of $$\hat{f}$$.
This is remarkable in that an uncountable set of numbers (all the values taken by $$f_L$$ over one
period) can be determined by a countable one (the samples of $$\hat{f}$$). Even more incredible, if
$$f$$ has finite bandwidth then only a finite number of the samples will be nonzero. So the
uncountable set of numbers is determined by a finite one.


As an aside, by taking $$x = 0$$ we can derive the
[Poisson summation formula](https://en.wikipedia.org/wiki/Poisson_summation_formula):

$$
\sum_{n \in \mathbb{Z}} f(nL) = \frac{1}{L} \sum_{n \in \mathbb{Z}} \hat{f}(n / L)
$$


## Derivation of the Discrete Time Fourier Transform

We can apply the above results to $$\hat{f}$$ as well. Recall that the Fourier transform of
$$\hat{f}$$ is $$f(-x)$$. Thus the periodic summation of $$\hat{f}$$ is

$$
\begin{align*}
\hat{f}_L(x) &= \frac{1}{L} \sum_{n \in \mathbb{Z}} f(-n / L) e^{2 \pi i n x / L} \\
&= \frac{1}{L} \sum_{n \in \mathbb{Z}} f(n / L) e^{-2 \pi i n x / L}
\end{align*}
$$

This is precisely the definition of the discrete time Fourier transform.

If $$f$$ is time-limited, then we'll have only a finite number of nonzero samples. But then
$$\hat{f}$$ is necessarily not bandwidth limited, so the tails of $$\hat{f}$$ will overlap in the
periodic summation. On the other hand, if $$f$$ is bandwidth limited, for sufficiently large $$L$$
we can recover $$\hat{f}$$. To do so perfectly requires an infinite number of samples, but in
practice reasonably bandwidth limited signals can still be recovered quite well from a finite number
of samples.


## Interpretation of the Discrete Fourier Transform

### Forward DFT

Suppose we take samples of a function $$f$$  at integer multiples of a time $$T$$ (or distance,
etc.). As we saw above,

$$
\hat{f}_{1/T}(x) = T \sum_{n \in \mathbb{Z}} f(n T) e^{-2 \pi i n x T}
$$

So when $$f$$ is time limited such that only the $$N$$ samples $$f(0)$$, $$\ldots$$,
$$f((N - 1) T)$$ are nonzero,

$$
\hat{f}_{1/T}(x) = T \sum_{n = 0}^{N - 1} f(n T) e^{-2 \pi i n x T}
$$

In this case the discrete Fourier transform gives us $$N$$ evenly spaced samples from one period of
$$\hat{f}_{1/T}$$. Namely, for $$0 \leq k < N$$,

$$
\hat{f}_{1/T}(k / NT) = T \sum_{n = 0}^{N - 1} f(n T) e^{-2 \pi i n k / N}
$$

### Backward DFT

Similarly,

$$
f_{NT}(x) = \frac{1}{NT} \sum_{n \in \mathbb{Z}} \hat{f}(n / NT) e^{2 \pi i n x / (NT)}
$$

So when $$f$$ is bandwidth limited such that only the $$N$$ samples $$\hat{f}(0)$$, $$\ldots$$,
$$\hat{f}((N - 1) / NT)$$ are nonzero,

$$
f_{NT}(x) = \frac{1}{NT} \sum_{n = 0}^{N - 1} \hat{f}(n / NT) e^{2 \pi i n x / (NT)}
$$

And in this case the inverse discrete Fourier transform gives us $$N$$ evenly spaced samples from
one period of $$f_{NT}$$. Namely, for $$0 \leq k < N$$,

$$
f_{NT}(kT) = \frac{1}{NT} \sum_{n = 0}^{N - 1} \hat{f}(n / NT) e^{2 \pi i n k / N}
$$

### Commentary

If $$f$$ were both time and bandwidth limited as discussed above, then $$f_{NT} = f$$ and
$$\hat{f}_{1/T} = \hat{f}$$. So the samples of $$\hat{f}$$ we get from the forward DFT could be fed
into the backward DFT to exactly recover our original samples of $$f$$. Unfortunately no such
functions exist, but in practice it works pretty well for signals that strike a balance between time
and bandwidth limits. The penalty for the imprecision is some aliasing caused by overlapping tails
in the periodic summations. So make sure $$1/T$$ is large enough to avoid significant aliasing in
the frequency domain, and $$NT$$ is large enough to avoid significant aliasing in the time (or
space, etc. domain).

We also see why the frequency spectra obtained from the DFT is periodic: we're not getting samples
of $$\hat{f}$$, but of its periodic summation. If $$\hat{f}$$ is localized near the origin, it
can be more instructive to view half the samples of $$\hat{f}_{1/T}$$ provided by the DFT as
representing positive frequencies, and the other half as negative frequencies.
