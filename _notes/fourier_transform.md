---
title: Fourier Transforms
---


## Definition

For a suitable function $$f : \mathbb{R} \rightarrow \mathbb{C}$$, the Fourier transform and inverse
Fourier transform are defined to be

$$
\begin{align*}
(\mathcal{F} f)(\xi) &= \int_\mathbb{R} f(x) e^{-2 \pi i x \xi} \mathrm{d} x \\
(\mathcal{F}^{-1} f)(x) &= \int_\mathbb{R} f(\xi) e^{2 \pi i \xi x} \mathrm{d} \xi
\end{align*}
$$

The Fourier transform of $$f$$ is frequently written as $$\hat{f}(\xi) = (\mathcal{F} f)(\xi)$$.

Every function in [$$L^1$$](https://en.wikipedia.org/wiki/Lp_space#Lp_spaces) has a Fourier
transform and inverse Fourier transform, since

$$
\begin{align*}
\left \vert \hat{f}(\xi) \right \vert
&\leq \int_\mathbb{R} \left \vert f(x) e^{-2 \pi i x \xi} \right \vert \mathrm{d} x \\
&= \int_\mathbb{R} \left \vert f(x) \right \vert \mathrm{d} x
\end{align*}
$$

Furthermore when $$f$$ is in $$L^1$$, then $$\hat{f}(\xi)$$ is a uniformly continuous function that
tends to zero as $$|\xi|$$ approaches infinity. However $$\hat{f}$$ need not be in $$L^1$$, and not
every continuous function that tends to zero is the Fourier transform of a function in $$L^1$$
(indeed describing $$\mathcal{F}(L^1)$$ is an open problem). As such it can be helpful to restrict
the definition to the [Schwartz space](https://en.wikipedia.org/wiki/Schwartz_space) over
$$\mathbb{R}$$, where the Fourier transform is an
[automorphism](https://en.wikipedia.org/wiki/Automorphism).

On the other hand, we'll also want to talk about the Fourier transforms of functions that aren't
absolutely integrable, or objects that aren't functions at all (like the [delta
function](https://en.wikipedia.org/wiki/Dirac_delta_function)). So I will tend to be very liberal
with my application of the transform.


## Basic Properties

The [Fourier Inversion Theorem](https://en.wikipedia.org/wiki/Fourier_inversion_theorem) states that
$$\mathcal{F} \mathcal{F}^{-1} = \mathcal{F}^{-1} \mathcal{F} = \mathcal{I}$$ (where $$\mathcal{I}$$
is the identity operator). This is strictly true for functions in $$L^1$$ whose transforms are also
in $$L^1$$, but can also be extended to more general spaces as well.

The Fourier transform is linear:

$$
\mathcal{F(af + bg)} = a \mathcal{F} f + b \mathcal{F} g
$$

If you shift everything in the original basis (usually the time or space domain), you pick up a
phase shift in the transformed (i.e. frequency) basis. This follows from a change of variables.

$$
\begin{align*}
(\mathcal{F} f(x + x_0))(\xi)
&= \int_\mathbb{R} f(x + x_0) e^{-2 \pi i x \xi} \mathrm{d} x \\
&= \int_\mathbb{R} f(x) e^{-2 \pi i (x - x_0) \xi} \mathrm{d} x \\
&= e^{2 \pi i x_0 \xi} \int_\mathbb{R} f(x) e^{-2 \pi i x \xi} \mathrm{d} x \\
&= e^{2 \pi i x_0 \xi} \hat{f}(\xi)
\end{align*}
$$

The reverse is also true (with a sign difference):

$$
\begin{align*}
\mathcal{F}(e^{2 \pi i x \xi_0} f(x))(\xi)
&= \int_\mathbb{R} e^{2 \pi i x \xi_0} f(x) e^{-2 \pi i x \xi} \mathrm{d} x \\
&= \int_\mathbb{R} f(x) e^{-2 \pi i x (\xi - \xi_0)} \mathrm{d} x \\
&= \hat{f}(\xi - \xi_0)
\end{align*}
$$

If you expand $$f$$ horizontally, you contract $$\hat{f}$$ both horizontally and vertically.

$$
\begin{align*}
\mathcal{F}(f(a x))(\xi)
&= \int_\mathbb{R} f(a x) e^{-2 \pi i x \xi} \mathrm{d} x \\
&= \frac{1}{|a|} \int_\mathbb{R} f(x) e^{-2 \pi i x \xi / a} \mathrm{d} x \\
&= \frac{1}{|a|} \hat{f} \left( \frac{\xi}{a} \right)
\end{align*}
$$


## Fourier Flips

The Fourier transform has a number of interesting properties related to the flip (or reversal)
operator $$(\mathcal{R} f)(x) = f(-x)$$. By definition

$$
(\mathcal{F}^{-1} f)(x) = \int_\mathbb{R} f(x') e^{2 \pi i x' x} \mathrm{d} x'
= (\mathcal{R} \mathcal{F} f)(x)
$$

And by changing the variable of integration from $$x'$$ to $$-x'$$

$$
\begin{align*}
(\mathcal{F}^{-1} f)(x)
&= \int_\mathbb{R} f(x') e^{2 \pi i x' x} \mathrm{d} x' \\
&= \int_\mathbb{R} f(-x') e^{-2 \pi i x' x} \mathrm{d} x' \\
&= (\mathcal{F} \mathcal{R} f)(x)
\end{align*}
$$

Thus $$\mathcal{F}^{-1} = \mathcal{F} \mathcal{R} = \mathcal{R} \mathcal{F}$$. (You can also derive
this using the expansion/contraction formula discussed above). This means that

$$
\mathcal{I}
= \mathcal{F} \mathcal{F}^{-1}
= \mathcal{F} \mathcal{F} \mathcal{R}
= \mathcal{R} \mathcal{F} \mathcal{F}
$$

and

$$
\mathcal{I}
= (\mathcal{F} \mathcal{F}^{-1}) (\mathcal{F} \mathcal{F}^{-1})
= \mathcal{F} \mathcal{F} \mathcal{F} \mathcal{F}
$$

So the Fourier transform generates cycles of functions. The forward transform takes you one forward,
while the inverse transform takes you one back.

$$
f(x)
\longrightarrow \hat{f}(x)
\longrightarrow f(-x)
\longrightarrow \hat{f}(-x)
\longrightarrow f(x)
$$


## Conjugate Symmetries

Recall that the [conjugate](https://en.wikipedia.org/wiki/Complex_conjugate) of a complex number
$$a + b i$$ is defined as $$\overline{a + bi} = a - bi$$. Conjugation distributes over addition and
multiplication. Knowing [Euler's formula](https://en.wikipedia.org/wiki/Euler%27s_formula) and that
cos(x) is even and sin(x) odd, it's easy to show that $$\overline{e^{ix}} = e^{-ix}$$ for
real $$x$$.

Let's see what the Fourier transform of the conjugate of a function looks like.

$$
\begin{align*}
(\mathcal{F} \overline{f})(x)
&= \int_\mathbb{R} \overline{f(x')} e^{-2 \pi i x' x} \mathrm{d} x' \\
&= \int_\mathbb{R} \overline{f(x') e^{2 \pi i x' x}} \mathrm{d} x' \\
&= \overline{\int_\mathbb{R} f(x') e^{2 \pi i x' x} \mathrm{d} x'} \\
&= \overline{(\mathcal{F}^{-1} f)(x)}
\end{align*}
$$

This implies that $$ \mathcal{F}^{-1} f$$ is the complex conjugate of $$\mathcal{F} \overline{f}$$.
So if we take the complex conjugate before and after, the forward Fourier transform becomes the
inverse Fourier transform. Let's redraw the arrow diagram from before as a
[commutative diagram](https://en.wikipedia.org/wiki/Commutative_diagram) that reflects this:

$$
\begin{array}{ccccccccc}
f(x) & \longrightarrow & \hat{f}(x) & \longrightarrow & f(-x) & \longrightarrow & \hat{f}(-x) & \longrightarrow & f(x) \\
\updownarrow && \updownarrow && \updownarrow && \updownarrow && \updownarrow \\
\overline{f(x)} & \longleftarrow & \overline{\hat{f}(x)} & \longleftarrow & \overline{f(-x)} & \longleftarrow & \overline{\hat{f}(-x)} & \longleftarrow & \overline{f(x)}
\end{array}
$$

In this version horizontal arrows indicate Fourier transforms (and you can always go backward along
a horizontal arrow by taking the inverse Fourier transform), and vertical arrows indicate complex
conjugates.


## Even More Odd

Recall that a function $$f$$ is even if $$f(-x) = f(x)$$, and odd if $$f(-x) = -f(x)$$. Note that a
function is real if and only if it's equal to its own complex conjugate, and a function is purely
imaginary if and only if its complex conjugate is its additive inverse. If any of these properties
hold, we can deduce some interesting things from the commutative diagram above.

- a function is even $$\iff$$ its Fourier transform is even
- a function is odd $$\iff$$ its Fourier transform is odd
- a function is real $$\iff$$ the conjugate of its transform is its inverse transform
  $$\iff$$ its transform is equal to its flipped and conjugated transform
- a function is real and even $$\iff$$ its transform is real and even
- a function is real and odd $$\iff$$ its transform is imaginary and odd

The case where $$f$$ is a real function is used often enough to be worth drawing out explicitly. It
also helps see the special cases for even and odd real functions. Here the diagonal arrows
represent Fourier transforms (as always they can be inverted), and the vertical arrow represents the
complex conjugate (which in this particular case is equivalent to the flip operator).

$$
\begin{array}{rcccl}
&& \hat{f}(x) = \overline{\hat{f}(-x)} && \\
& \nearrow && \searrow & \\
f(x) && \updownarrow && f(-x) \\
& \nwarrow && \swarrow & \\
&& \overline{\hat{f}(x)} = \hat{f}(-x) &&
\end{array}
$$

In particular, since conjugation doesn't change magnitude, the magnitude of the transform of a real
function is an even function. Because of this it's common to see the negative frequencies ignored
when we're dealing with a real function and only care about the magnitude of the transform (like
for spectral power analysis).


## The Transform of a Gaussian

A Fourier transform that comes up frequently is that of a Gaussian. It can be calculated by
completing a square.

$$
\begin{align*}
\mathcal{F}(N(0, \sigma^2))(f)
&= \frac{1}{\sqrt{2 \pi \sigma^2}} \int_{-\infty}^\infty e^{\frac{-x^2}{2 \sigma^2}}
    e^{- 2 \pi i f x} \mathrm{d} x \\
&= \frac{1}{\sqrt{2 \pi \sigma^2}} \int_{-\infty}^\infty e^{-\frac{1}{2 \sigma^2}
    \left( x^2 + 4 \pi i \sigma^2 f x \right)} \mathrm{d} x \\
&= \frac{1}{\sqrt{2 \pi \sigma^2}} \int_{-\infty}^\infty e^{-\frac{1}{2 \sigma^2}
    \left( x + 2 \pi i \sigma^2 f \right)^2 + \frac{(2 \pi i \sigma^2 f)^2}{2 \sigma^2}}
    \mathrm{d} x \\
&= e^{-2 \pi^2 f^2 \sigma^2} \frac{1}{\sqrt{2 \pi \sigma^2}} \int_{-\infty}^\infty
    e^{-\frac{\left( x + 2 \pi i \sigma^2 f \right)^2}{2 \sigma^2}} \mathrm{d} x \\
&= e^{-2 \pi^2 f^2 \sigma^2} \\
&= e^{-\frac{(2 \pi f)^2}{2 / \sigma^2}}
\end{align*}
$$

This is an unnormalized Gaussian with variance $$1/\sigma^2$$. Note that the exponent wants to be
expressed in radians instead of cycles, so $$f$$ is scaled by $$2 \pi$$.

This function integrates to $$(2 \pi \sigma^2)^{-1/2}$$. One might have hoped it would be
normalized. One reason this could not be true is Plancherel's Theorem. The "power" of a normalized
Gaussian is

$$
\begin{align*}
\int_{-\infty}^\infty \left( \frac{1}{\sqrt{2 \pi \sigma^2}} e^{\frac{-x^2}{2 \sigma^2}} \right)^2
\mathrm{d} x
&= \frac{1}{2 \pi \sigma^2} \int_{-\infty}^\infty e^{\frac{-x^2}{\sigma^2}} \mathrm{d} x \\
&= \frac{1}{2 \pi \sigma} \int_{-\infty}^\infty e^{-x^2} \mathrm{d} x \\
&= \frac{1}{2 \sqrt{\pi} \sigma}
\end{align*}
$$

This depends on the variance, which is inverted by the Fourier transform. So since the power is
invariant, the normalization cannot in general be conserved.
