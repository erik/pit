---
title: Fourier Examples
---

Working these out by hand helped me debug my CT algorithms.

![page_1](../assets/img/notes_ft_examples_1.jpg)
![page_2](../assets/img/notes_ft_examples_2.jpg)
![page_3](../assets/img/notes_ft_examples_3.jpg)
