---
title: Chapter 2 Notes
---

## Photon Shot Noise

The shot noise derivation in the book assumes a constant charge per particle. It also assumes that
the energy is proportional to the signal squared. Neither of these is true for photons, so I'm
curious how things look if we relax these assumptions.

Let's assume that each photon energies are normally distributed i.i.d. random variables. I'll still
model the arrival times with a Poisson distribution. Instead of electric current (in Amps), the
train of photon arrivals tells us directly the energy transmission per time (Watts).

$$
W(t) = \sum_{n = 1}^N q_n \delta(t - t_n)
$$

where $$q_n$$ is the energy of the $$n$$th photon, and $$t_n$$ its time of arrival. The Fourier
transform of this function is

$$
\begin{align*}
W(f) &= \int_\mathbb{R} e^{-2 \pi i t f} \sum_{n = 1}^N q_n \delta(t - t_n) \mathrm{d} t \\
&= \sum_{n = 1}^N q_n e^{-2 \pi i t_n f} \\
\end{align*}
$$

Each photon contributes one term to this sum. The phase of its contribution is determined by its
arrival time (and the frequency of interest), while the magnitude is determined by its energy.

We want to know the expected magnitude of this function.

$$
\begin{align*} \langle |W(f)| \rangle
&= \left \langle \left| \sum_{n = 1}^N q_n e^{-2 \pi i t_n f} \right| \right \rangle \\
&= \sqrt{N} \langle q \rangle \end{align*}
$$

Note: I think this is wrong... Formally the expectation should be taken over sequences of arrival
energies and times $$(q_1, t_1), \ldots, (q_n, t_N)$$. But in this case we can shortcut the math by
noticing we've ended up with a 2d Gaussian random walk. The arrival times determine direction
(phase), and the energies determine the step lengths. For a given frequency, all are uncorrelated in
expectation.

Let's look at the expected squared magnitude as well, so we can compare with the standard shot noise
formula. Just note that this is not the spectral power density in this case. It's the spectral
density of Watts squared which doesn't have any particularly useful physical meaning.

$$
\begin{align*}
\left \langle |W(f)|^2 \right \rangle
&= \left \langle \sum_{n = 1}^N q_n e^{-2 \pi i t_n f}
                 \sum_{m = 1}^N q_m e^{2 \pi i t_m f} \right \rangle \\
&= \left \langle \sum_{n = 1}^N \sum_{m = 1}^N q_n q_m e^{-2 \pi i (t_n - t_m) f} \right \rangle \\
&= \left \langle \sum_{n = 1}^N q_n^2 + \sum_{n < m} 2 q_n q_m
   \text{Re} \left[ e^{-2 \pi i (t_n - t_m) f} \right] \right \rangle \\
&= \sum_{n = 1}^N \left \langle q_n^2 \right \rangle + 2 \sum_{n < m} \left \langle q_n q_m
   \text{Re} \left[ e^{-2 \pi i (t_n - t_m) f} \right] \right \rangle \\
&= N \left \langle q^2 \right \rangle
\end{align*}
$$

The second term drops out because the arrival times are generated by a Poisson process, so the real
part of the phase differences averages out to zero.
